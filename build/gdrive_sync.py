# coding: utf-8
# Outils pour modifier les fichiers de références vers les fichiers GDrive
# pour modifier les dates de ces derniers, afin d'être identifié comme modifié pour le Makefile²

import json
import os.path
import pickle
import sys
from datetime import datetime
from pathlib import Path

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

# If modifying these scopes, delete the file token.pickle.
# SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly']
SCOPES = ['https://www.googleapis.com/auth/drive']


def authent():
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(str(Path.home()) + '/.token_gdrive.pickle'):
        with open(str(Path.home()) + '/.token_gdrive.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                str(Path.home()) + '/credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(str(Path.home()) + '/.token_gdrive.pickle', 'wb') as token:
            pickle.dump(creds, token)
    return creds


def main():
    creds = authent()

    drive_service = build('drive', 'v3', credentials=creds)

    if len(sys.argv) < 2:
        return  # Juste l'authentification

    root_path = sys.argv[1]
    mask = sys.argv[2]

    gdrive_files = drive_service.files()
    import glob
    dirList = glob.glob(root_path + "/**/"+mask)
    for filename in dirList:
        with open(filename, 'r') as file:
            try:
                ref = json.loads(file.readline())
                file_id = ref["file_id"]
                request = drive_service.files().get(fileId=file_id, fields="modifiedTime")
                modified_time = request.execute()["modifiedTime"]
                dt = int(datetime.strptime(modified_time, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp())
                mtime = int(os.path.getmtime(filename))
                if mtime != dt:
                    try:
                        os.utime(filename, times=(dt, dt))
                    except PermissionError:
                        Path(filename).touch()  # Work around
                    print(f"touch {filename}")
            except ValueError:
                print(f"Impossible to parse {filename}")
    # request = drive_service.files().export_media(fileId=file_id,
    #                                              mimeType=mime_type)
    # print(f"Download {file_id} to {output_file}")
    # fh = open(output_file, 'wb')
    # downloader = MediaIoBaseDownload(fh, request)
    # done = False
    # while done is False:
    #     status, done = downloader.next_chunk()
    #     "Download %d%%." % int(status.progress() * 100)


if __name__ == '__main__':
    main()
