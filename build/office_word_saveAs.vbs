' Converti les fichiers PowerPoint vers différents formats
' Author: Philippe PRADOS

' https://docs.microsoft.com/fr-fr/office/vba/api/word.wdsaveformat
Dim mapMode
Set mapMode = CreateObject("Scripting.Dictionary")
mapMode.CompareMode = vbTextCompare
mapMode.Add "wdFormatDocument", 0
mapMode.Add "wdFormatDOSText", 4
mapMode.Add "wdFormatDOSTextLineBreaks", 5
mapMode.Add "wdFormatEncodedText", 7
mapMode.Add "wdFormatFilteredHTML",10
mapMode.Add "wdFormatFlatXML",19
mapMode.Add "wdFormatFlatXMLMacroEnabled",20
mapMode.Add "wdFormatFlatXMLTemplate",21
mapMode.Add "wdFormatFlatXMLTemplateMacroEnabled",22
mapMode.Add "wdFormatOpenDocumentText",23
mapMode.Add "wdFormatHTML",8
mapMode.Add "wdFormatRTF",6
mapMode.Add "wdFormatStrictOpenXMLDocument",24
mapMode.Add "wdFormatTemplate",1
mapMode.Add "wdFormatText",2
mapMode.Add "wdFormatTextLineBreaks",3
mapMode.Add "wdFormatUnicodeText",7
mapMode.Add "wdFormatWebArchive",9
mapMode.Add "wdFormatXML",11
mapMode.Add "wdFormatDocument97",0
mapMode.Add "wdFormatDocumentDefault",16
mapMode.Add "wdFormatPDF",17
mapMode.Add "wdFormatTemplate97",1
mapMode.Add "wdFormatXMLDocument",12
mapMode.Add "wdFormatXMLDocumentMacroEnabled",13
mapMode.Add "wdFormatXMLTemplate",14
mapMode.Add "wdFormatXMLTemplateMacroEnabled",15
mapMode.Add "wdFormatXPS",18

Set args=WScript.Arguments

Dim filename_source, filename_dest, mode
filename_source = args.item(0)
filename_dest = args.item(1)
mode = mapMode(args.item(2))

'WScript.Echo "source=" & filename_source
'WScript.Echo "dest=" & filename_dest

Set word = CreateObject("Word.Application")
Set document = word.Documents.Open(filename_source)
if err.Number <> 0 then : WScript.Quit(-1) : end if
document.SaveAs2 filename_dest, mode
if err.Number <> 0 then : WScript.Quit(-1) : end if
document.close()

