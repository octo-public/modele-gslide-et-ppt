# coding: utf-8
from __future__ import print_function

import io
import pickle
import os.path
from pathlib import Path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaIoBaseDownload
import sys

# If modifying these scopes, delete the file token.pickle.
#SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly']
SCOPES = ['https://www.googleapis.com/auth/drive']
def authent():
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(str(Path.home())+'/.token_gdrive.pickle'):
        with open(str(Path.home())+'/.token_gdrive.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                str(Path.home())+'/credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(str(Path.home())+'/.token_gdrive.pickle', 'wb') as token:
            pickle.dump(creds, token)
    return creds



def main():
    creds = authent()

    drive_service = build('drive', 'v3', credentials=creds)

    if len(sys.argv) < 2:
        return  # Juste l'authentification

    file_id = sys.argv[1]
    output_file  = sys.argv[2]
    mime_type = sys.argv[3]

    request = drive_service.files().export_media(fileId=file_id,
                                                 mimeType=mime_type)
    print(f"Download {file_id} to {output_file}")
    fh = open(output_file, 'wb')
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        "Download %d%%." % int(status.progress() * 100)

if __name__ == '__main__':
    main()