@chcp 1252 2>nul >nul
rem @echo off
REM Install on Windows
REM Author: Philippe PRADOS
SET MODE=%1

REM Custom Office Templates
SET MILLESIME=2020-04
SET OCTO_TEMPLATES=C:\ProgramData\OCTO\Template
SET SLIDES_PREFORMATE=Slides_pr�format�s
SET THEME=%OCTO_TEMPLATES%\Themes
rem SET ADDINS=%APPDATA%\Microsoft\AddIns
SET ADDINS=%OCTO_TEMPLATES%\%MILLESIME%

Rem Check Code page
chcp 2>nul >nul
set cp=
reg query HKEY_CURRENT_USER\Software\Wine >nul 2>nul
IF %ERRORLEVEL% EQU 0 goto WINE
for /f "tokens=3 delims=:." %%x in ('chcp') do set cp=%%x
set cp=%cp: =%

IF "%MODE%"=="" (
  SET MODE=links
)
IF "%MODE%"=="LINKS" (
    set MODE=links
) ELSE IF "%MODE%"=="COPY" (
    set MODE=copy
) ELSE IF "%MODE%"=="copy" (
    set MODE=copy
)

rmdir /Q "%OCTO_TEMPLATES%\%MILLESIME" 2>nul

mkdir "%OCTO_TEMPLATES%" 2>nul
IF "%MODE%"=="links" (
    echo Use links
    powershell Start-Process cmd.exe -Verb runAs ^
    -Arg '/c ^
    mklink /d """"%OCTO_TEMPLATES%\%MILLESIME%""""            """"%CD%"""" ^& ^
    mklink """"%ADDINS%\%MILLESIME%_OCTO_Tools.ppam""""    """"%OCTO_TEMPLATES%\%MILLESIME%\%MILLESIME%_OCTO_Tools.ppam"""" ^& ^
        '
) ELSE IF "%MODE%"=="copy" (
    echo Use copy
    copy /Y "%CD%\%MILLESIME%_OCTO_Tools.ppam"               "%ADDINS%"
    xcopy /Q /I /S /Y "%CD%"                                 "%OCTO_TEMPLATES%\%MILLESIME%"
)
:WINE

REM Detection de la langue d'Office
c:\Windows\system32\cscript.exe /nologo bin\office_powerpoint_install_addins.vbs "%ADDINS%\%MILLESIME%_OCTO_Tools.ppam"
for /F "tokens=3" %%A in ('reg query "HKCU\Software\Microsoft\Office\16.0\Common\LanguageResources" /v "UILanguageTag"') DO (set "RegKey=%%A")
for /f "tokens=1,2 delims=-" %%a in ("%RegKey%") do set OFFICE_LANG=%%a

reg add HKCU\Software\Microsoft\Office\16.0\Common\Spotlight\Providers\OCTO ^
    /f ^
    /v ServiceURL ^
    /t REG_SZ ^
    /d C:\ProgramData\OCTO\Template\2020-04\bin\OCTO_Templates_%OFFICE_LANG%.xml >nul
reg add HKCU\Software\Microsoft\Office\16.0\Common\Spotlight\Providers\OCAC ^
    /f ^
    /v ServiceURL ^
    /t REG_SZ ^
    /d C:\ProgramData\OCTO\Template\2020-04\bin\OCAC_Templates_%OFFICE_LANG%.xml >nul
echo OCTO Tools installed

endlocal
:end
