#!/usr/bin/env bash
# Install on Wine or CrossOver
# Author: Philippe PRADOS
set -e
cd "$(dirname $0)"

# Check the presence of Wine
which wine >/dev/null || ( echo "Install Wine or Crossover before, and Office 365 on a bottle" 1>&2 ; exit 1 )

export WINEPREFIX="${WINEPREFIX:~/.wine}"

# Check if CrossOver
if [[ -e ~/.cxoffice ]] ; then
# Assume qu'une seule bouteille existe avec le mot 'Office'
export WINEPREFIX=$(ls -d ~/.cxoffice/*Office*)
export CX_ROOT="${CX_ROOT:-$(dirname "$WINEPREFIX")}"
export CX_BOTTLE="${CX_BOOTLE:-$(basename "$WINEPREFIX")}"
fi

MODE=${1:-links}

MILLESIME=2020-04
DRIVE_C=${WINEPREFIX}/drive_c
OCTO_TEMPLATES=${DRIVE_C}/ProgramData/OCTO/Template/${MILLESIME}
SLIDES_PREFORMATE=Slides_préformatés

wine cmd /c "$(realpath uninstall-windows.bat)"

rm -f "${OCTO_TEMPLATES}"
